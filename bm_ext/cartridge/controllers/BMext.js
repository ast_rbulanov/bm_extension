var ISML = require('dw/template/ISML'),
	URLUtils = require('dw/web/URLUtils'),
	Resource = require('dw/web/Resource'),
	Transaction = require('dw/system/Transaction'),
	Request = require('dw/system/Request'),
	Site = require('dw/system/Site').getCurrent(),
    SystemObjectMgr = require('dw/object/SystemObjectMgr');

var group = 'TReuters';
var sitepref =  Site.getPreferences().getCustom();
var preferences = SystemObjectMgr.describe('SitePreferences').getAttributeGroup(group).getAttributeDefinitions();
// render dashboard
function Start(){
	var params = request.httpParameterMap.getParameterMap('custom_');
	error = 'false';
	if(params.save.value == 1){
		handleForm(params);
	}
	
	ISML.renderTemplate('bmext', {
		sitepref: sitepref,
		preferences: preferences,
		group: group,
		error: error
		});
 	}

function handleForm(params) {
	if (params.getParameterCount() > 1) {
		var prefs = preferences.iterator();
		while(prefs.hasNext()){
			var pref =  prefs.next();
			var id = pref.getID();
			var type = pref.valueTypeCode;
			var value = 'my_bm_extension_default_value';
			switch (type) {
			// string, text
			case 4:
			case 3:
				value = params[id].value;
				break;
			// boolean
			case 8:
				value = params[id].value == 'on' ? true : false;  
				break;
			// enum of strings
			case 23:
				value = params[id].value.split(',');
				break;
			// set of strings	
			case 33:
				value = params[id].value;
				break;	
			default:
				break;
			}
			
			if(value !== 'my_bm_extension_default_value'){
				Transaction.wrap(function(){
					Site.setCustomPreferenceValue(id, value);	
				})
			}
		}
	}
}

exports.Start = Start;
exports.Start.public = true;